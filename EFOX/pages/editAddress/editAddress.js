const app = getApp()
const gb = app.globalData

Page({
    data: {
        isShowPopup: false,
        selectProvinceId: null,
        selectCityId: null,
        selectDistrictId: null,
        name: '',
        phoneNumber: '',
        addressId: '',
        addressDetail: '',
        detail: '',
        provincelist: [],
        userId: '',
        citylist: [
            {
                id: 0,
                name: "请选择"
            }
        ],
        districtlist: [
            {
                id: 0,
                name: "请选择"
            }
        ]
    },
    inputName(e){
        this.setData({
            name: e.detail.value
        })
    },
    inputPhone(e){
        this.setData({
            phoneNumber: e.detail.value
        })
    },
    inputAddrDetail(e){
        this.setData({
            addressDetail: e.detail.value
        })
    },
    onLoad() {
        my.getStorage({
          key: 'userId',
          success: (result) => {
            this.setData({
                userId: result.data.userId
            })
          }
        });
        my.request({
            url: gb.url + '/address/getAddressList',
            method: 'GET',
            data: {
                id: 0
            },
            success: (result) => {
                this.setData({
                    provincelist: result.data.data
                })
            },
            fail: () => {
                
            },
            complete: () => {
                
            }
        });
    },
    isShowPicker(){
        console.log("展示选择器")
        this.setData({
            isShowPopup: true
        })
    },
    closePopup(){
        this.setData({
            isShowPopup: false
        })
    },
    pickChange(e){
        console.log(e.detail.value);
        var arr = e.detail.value;
        
        my.request({
            url: gb.url + '/address/getAddressList',
            method: 'GET',
            data: {
                id: this.data.provincelist[arr[0]].id
            },
            success: (result) => {
                    this.setData({
                        citylist: result.data.data
                    })
                    my.request({
                        url: gb.url + '/address/getAddressList',
                        method: 'GET',
                        data: {
                            id: this.data.citylist[arr[1]].id
                        },
                        success: (result) => {
                                this.setData({
                                    districtlist: result.data.data
                                })
                        }
                    });
            },
            complete: ()=>{
                this.setData({
                    addressId: this.data.districtlist[arr[2]].id,
                    detail: this.data.provincelist[arr[0]].name +" "+ this.data.citylist[arr[1]].name +" "+ this.data.districtlist[arr[2]].name
                })
            }
        });
        
    },
    chooseLocation(){
      var that = this
      my.chooseLocation({
        success: (res) => {
          console.log(res)
          that.setData({
            addressDetail: res.address+res.name
          })
        }
      })
    },
    confirmAddressId(){
        this.setData({
            isShowPopup: false
        })
    },
    confirm(){
        let name = this.data.name;
        let phoneNumber = this.data.phoneNumber;
        let addressId = this.data.addressId;
        let addressDetail = this.data.addressDetail;
        let userId = this.data.userId;
        
        if(name!=''&&phoneNumber!=''&&addressId!=''&addressDetail!=''){
            my.request({
              url: gb.url + '/address/insertAddress',
              method: 'POST',
              data: {
                userId: userId,
                name: name,
                phoneNumber: phoneNumber,
                addressId: addressId,
                addressDetail: addressDetail
              },
              success: (result) => {
                my.showToast({
                  type: 'success',
                  content: '添加成功',
                  duration: 1000,
                  success: () => {
                    my.navigateBack()
                  },
                  fail: () => {
                    
                  },
                  complete: () => {
                    
                  }
                });
              }
            });
        }else{
            my.alert({
              title: '结果',
              content: '提交失败，请检查输入',
              buttonText: '确定'
            });
        }
    }
});
