const app = getApp()
const gb = app.globalData

Page({
  data: {
    userId: '',
    consigneeName: '',
    consigneePhone: '',
    consigneeAddr: '',
    cDetail: '',
    consigneeDetail: '',
    senderName: '',
    serderPhone: '',
    senderAddr: '',
    sDetail: '',
    serderDetail: '',
    sendMode: 0,
    paymentMode: 0,
    payment: "支付宝",
    send: "上门取件",
    isShowPopup1: false,
    isShowPopup2: false,
    showPopPayment: false,
    showPopSend: false,
    maskPayment: false,
    maskSend: false,
    provinceList: [],
    payOption: [
      {
        name: "支付宝",
        value: 0,
        checked: true,
      },
      {
        name: "微信",
        value: 1,
        checked: false,
      },
      {
        name: "取现金",
        value: 2,
        checked: false,
      }
    ],
    sendOption: [
      {
        name: "上门取件",
        value: 0,
        checked: true,
      },
      {
        name: "自寄",
        value: 1,
        checked: false
      }
    ],
    cityList: [
      {
                id: 0,
                name: "请选择"
            }
    ],
    districtList: [
      {
                id: 0,
                name: "请选择"
            }
    ]
  },
  closePopup(){
    this.setData({
      isShowPopup1: false,
      isShowPopup2: false
    })
  },
  isShowPayment(){
    let payment = this.data.showPopPayment;
    let send = this.data.showPopSend;
    if(payment == false){
      if(send == true){
        this.setData({
          showPopSend: !this.data.showPopSend
        })
      }
    }
    this.setData({
      showPopPayment: !this.data.showPopPayment
    })
  },
  isShowSend(){
    let payment = this.data.showPopPayment;
    let send = this.data.showPopSend;
    if(send == false){
      if(payment == true){
        this.setData({
          showPopPayment: !this.data.showPopPayment
        })
      }
    }
    this.setData({
      showPopSend: !this.data.showPopSend
    })
  },
  isShowPicker1(){
    this.setData({
      isShowPopup1: true
    })
  },
  isShowPicker2(){
    this.setData({
      isShowPopup2: true
    })
  },
  confirmAddressId1(){
      this.setData({
          isShowPopup1: false
      })
  },
  confirmAddressId2(){
      this.setData({
          isShowPopup2: false
      })
  },
  chooseLocation1(){
    var that = this
    my.chooseLocation({
      success: (res) => {
        console.log(res)
        that.setData({
          serderDetail: res.address+res.name
        })
      }
    })
  },
  chooseLocation2(){
    var that = this
    my.chooseLocation({
      success: (res) => {
        console.log(res)
        that.setData({
          consigneeDetail: res.address+res.name
        })
      }
    })
  },
  paymentChange(e){
    if(e.detail.value == 0){
      this.setData({
        paymentMode: this.data.payOption[e.detail.value].value,
        payment: this.data.payOption[e.detail.value].name,
        showPopPayment: false,
        payOption: [
          {
            name: "支付宝",
            value: 0,
            checked: true,
          },
          {
            name: "微信",
            value: 1,
            checked: false,
          },
          {
            name: "取现金",
            value: 2,
            checked: false,
          }
        ]
      })
    }
    if(e.detail.value == 1){
      this.setData({
        paymentMode: this.data.payOption[e.detail.value].value,
        payment: this.data.payOption[e.detail.value].name,
        showPopPayment: false,
        payOption: [
          {
            name: "支付宝",
            value: 0,
            checked: false,
          },
          {
            name: "微信",
            value: 1,
            checked: true,
          },
          {
            name: "取现金",
            value: 2,
            checked: false,
          }
        ]
      })
    }
    if(e.detail.value == 2){
      this.setData({
        paymentMode: this.data.payOption[e.detail.value].value,
        payment: this.data.payOption[e.detail.value].name,
        showPopPayment: false,
        payOption: [
          {
            name: "支付宝",
            value: 0,
            checked: false,
          },
          {
            name: "微信",
            value: 1,
            checked: false,
          },
          {
            name: "取现金",
            value: 2,
            checked: true,
          }
        ]
      })
    }
    console.log(e)
  },
  sendChange(e){
    if(e.detail.value == 0){
      this.setData({
        sendMode: this.data.sendOption[e.detail.value].value,
        send: this.data.sendOption[e.detail.value].name,
        showPopSend: false,
        sendOption: [
          {
            name: "上门取件",
            value: 0,
            checked: true,
          },
          {
            name: "自寄",
            value: 1,
            checked: false
          }
        ]
      })
    }
    if(e.detail.value == 1){
      this.setData({
        sendMode: this.data.sendOption[e.detail.value].value,
        send: this.data.sendOption[e.detail.value].name,
        showPopSend: false,
        sendOption: [
          {
            name: "上门取件",
            value: 0,
            checked: false,
          },
          {
            name: "自寄",
            value: 1,
            checked: true
          }
        ]
      })
    }
    console.log(e)
  },
  inputName1(e){
      this.setData({
          senderName: e.detail.value
      })
  },
  inputPhone1(e){
      this.setData({
          serderPhone: e.detail.value
      })
  },
  inputAddrDetail1(e){
      this.setData({
          serderDetail: e.detail.value
      })
  },
  inputName2(e){
      this.setData({
          consigneeName: e.detail.value
      })
  },
  inputPhone2(e){
      this.setData({
          consigneePhone: e.detail.value
      })
  },
  inputAddrDetail2(e){
      this.setData({
          consigneeDetail: e.detail.value
      })
  },
  pickChange1(e){
      console.log(e.detail.value);
      var arr = e.detail.value;
      
      my.request({
          url: gb.url + 'address/getAddressList',
          method: 'GET',
          data: {
              id: this.data.provinceList[arr[0]].id
          },
          success: (result) => {
                  this.setData({
                      cityList: result.data.data
                  })
                  my.request({
                      url: gb.url + 'address/getAddressList',
                      method: 'GET',
                      data: {
                          id: this.data.cityList[arr[1]].id
                      },
                      success: (result) => {
                              this.setData({
                                  districtList: result.data.data
                              })
                      }
                  });
          },
          complete: ()=>{
              this.setData({
                  senderAddr: this.data.districtList[arr[2]].id,
                  sDetail: this.data.provinceList[arr[0]].name +" "+ this.data.cityList[arr[1]].name +" "+ this.data.districtList[arr[2]].name
              })
          }
      });
      
  },
  pickChange2(e){
      console.log(e.detail.value);
      var arr = e.detail.value;
      
      my.request({
          url: gb.url + 'address/getAddressList',
          method: 'GET',
          data: {
              id: this.data.provinceList[arr[0]].id
          },
          success: (result) => {
                  this.setData({
                      cityList: result.data.data
                  })
                  my.request({
                      url: gb.url + 'address/getAddressList',
                      method: 'GET',
                      data: {
                          id: this.data.cityList[arr[1]].id
                      },
                      success: (result) => {
                              this.setData({
                                  districtList: result.data.data
                              })
                      }
                  });
          },
          complete: ()=>{
              this.setData({
                  consigneeAddr: this.data.districtList[arr[2]].id,
                  cDetail: this.data.provinceList[arr[0]].name +" "+ this.data.cityList[arr[1]].name +" "+ this.data.districtList[arr[2]].name
              })
          }
      });
      
  },
  createOrder(){
    let userId=this.data.userId
    let consigneeName=this.data.consigneeName
    let consigneePhone= this.data.consigneePhone
    let consigneeAddr= this.data.consigneeAddr
    let consigneeDetail= this.data.consigneeDetail
    let senderName= this.data.senderName
    let serderPhone= this.data.serderPhone
    let senderAddr= this.data.senderAddr
    let serderDetail= this.data.serderDetail
    let sendMode= this.data.sendMode
    let paymentMode= this.data.paymentMode
    my.getStorage({
      key: 'userId',
      success: (result) => {
        console.log(result)
        if(result.data.userId){
          if(consigneeName!=''&consigneePhone!=''&consigneeAddr!=''&consigneeDetail!=''&senderName!=''&serderPhone!=''&senderAddr!=''&serderDetail!=''){
            console.log("ok")
            my.request({
              url: gb.url + 'order/createOrder',
              method: 'POST',
              data: {
                userId: result.data.userId,
                consigneeName: consigneeName,
                consigneePhone: consigneePhone,
                consigneeAddr: consigneeAddr,
                consigneeDetail: consigneeDetail,
                senderName: senderName,
                serderPhone: serderPhone,
                senderAddr: senderAddr,
                serderDetail: serderDetail,
                sendMode: sendMode,
                paymentMode: paymentMode
              },
              success: (result) => {
                my.showToast({
                  type: 'success',
                  content: '下单成功！',
                  duration: 1000
                });
                this.setData({
                  consigneeName: '',
                  consigneePhone: '',
                  consigneeAddr: '',
                  cDetail: '',
                  consigneeDetail: '',
                  senderName: '',
                  serderPhone: '',
                  senderAddr: '',
                  sDetail: '',
                  serderDetail: ''
                })
              },
              fail: () => {
                my.showToast({
                  type: "fail",
                  content: "失败，请重试",
                  duration: 1000
                })
              }
            });
          }else{
            my.showToast({
              type: 'fail',
              content: '请检查输入',
              duration: 1000
            });
          }
        }else{
          my.showToast({
            type: 'warn',
            content: '请先登录',
            duration: 1000
          })
        }
      },
      fail: () => {
        my.showToast({
          type: 'warn',
          content: '请先登录',
          duration: 1000
        })
      }
    });
    
  },
  onLoad() {
    my.setStorage({
      key: 'userId',
      data: {
        userId: ''
      }
    });
    my.getStorage({
          key: 'userId',
          success: (result) => {
            console.log(result)
            this.setData({
                userId: result.data.userId
            })
          },
          fail: (result) => {
            console.log('用户暂未登录')
          }
        });
        my.request({
            url: gb.url + 'address/getAddressList',
            method: 'GET',
            data: {
                id: 0
            },
            success: (result) => {
                this.setData({
                    provinceList: result.data.data
                })
            }
        });
  }
});