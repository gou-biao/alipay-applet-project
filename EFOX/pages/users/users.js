const app = getApp();
const gb = app.globalData;

Page({
  data: {
    avatar:"",
    nickName:"",
    myOrderFunc: {
      icon: "check_",
      size: "50",
      title: "我的订单",
      navigateTo: "ToOrderList"
    },
    addressBookFunc: {
      icon: "address-book_",
      size: "50",
      title: "地址簿",
      navigateTo: "ToAddressBook"
    },
    canIUseAuthButton: true,
    showUserCard: false
  },
  ToOrderList(){
    my.navigateTo({ url: '/pages/orderList/orderList' })
  },
  ToAddressBook(){
    my.navigateTo({url: '/pages/address/address'})
  },
  onGetAuthorize(res) {
    my.getOpenUserInfo({
      success: (res) => {
        let userNickName = JSON.parse(res.response).response.nickName
        let userAvatar = JSON.parse(res.response).response.avatar
        let userGender = JSON.parse(res.response).response.gender
        this.setData({
          canIUseAuthButton: false,
          showUserCard: true,
          nickName: userNickName,
          avatar: userAvatar
        })
        my.setStorage({
          key: 'userAvatarUrl',
          data: {
              avatar: this.data.avatar
          }
        });
        my.setStorage({
          key: 'userNickName',
          data: {
            nickName: this.data.nickName
          },
          success: () => {
            console.log("success")
          },
          fail: () => {
            console.log("fail")
          }
        })
        my.getAuthCode({
          scopes: 'auth_base',
          success: (result) => {
            if(result){
              console.log(result)
              my.request({
                url: gb.url + 'user/accessToken',
                data: {
                  authCode: result.authCode,
                  nickName: userNickName,
                  gender: userGender,
                  avatar: userAvatar
                },
                method: "POST",
                dataType: "json",
                success: (result) => {
                  console.log(result)
                  my.setStorage({
                    key: 'userId',
                    data: {
                      userId: result.data.data.userId
                    },
                    success: () => {
                      console.log("success")
                    },
                    fail: () => {
                      console.log("fail")
                    }
                  });
                },
                fail: (result) => {
                  my.alert({
                    content: "请重新登录"
                  })
                }
              })
            }
          },
        });
      },
      fail: function(res) {
        my.alert({content:"获取用户信息失败"})
      }
    });
      
  },
  onAuthError() {
    my.alert({
      content: "用户拒绝授权或发生错误"
    })
  },
  onLoad() {
    my.getSetting({
      success: (res) => {
        console.log(res);
        this.setData({
            canIUseAuthButton: false,
            showUserCard: true,
        })
        if(res.authSetting.userInfo){
          my.getAuthCode({
            scopes: 'auth_base',
            success: (result) => {
              if(result){
                console.log(result)
                my.request({
                  url: gb.url + 'user/getUserInfo',
                  data: {
                    authCode: result.authCode
                  },
                  method: "Get",
                  dataType: "json",
                  success: (result) => {
                    console.log(result)
                    let userNickName = result.data.data.userName;
                    let userAvatar = result.data.data.userIdnumber;
                    this.setData({
                      avatar: userAvatar, 
                      nickName: userNickName
                    })
                    my.setStorage({
                      key: 'userId',
                      data: {
                        userId: result.data.data.userId
                      }
                    });
                  },
                  fail: (result) => {
                    my.alert({
                      content: "请重新登录"
                    })
                    this.setData({
                      canIUseAuthButton: true,
                      showUserCard: false,
                    })
                  }
                })
              }
            },
          });
        }else{
          this.setData({
            canIUseAuthButton: true,
            showUserCard: false,
          })
        }
      }
    });
  },
});
