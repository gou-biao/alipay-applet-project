const app = getApp()
const gb = app.globalData

Page({
  data: {
      isShowDetail: false,
      orderNumber: "",
      senderCity: "",
        consigneeCity: "",
        transStatus: "4",
        centerLatitude: "",
        centerLongitude: "",
        carLatitude: "",
        carLongitude: "",
        consigneeLatitude: "",
        consigneeLongitude: "",
        markers: [],
        polylines: [],
        items:[],
        activeIndex: "1",
        failIndex: "0",
        showNumberSteps: true
  },
  onInputConfirm(e) {
    console.log(e)
    if(e){
      this.setData({
        orderNumber: e.detail.value
      })
    }    
    my.request({
      url: gb.url + '/user/selectOrderDetail',
      method: 'GET',
      data: {
          orderNumber: this.data.orderNumber
      },
      success: (result) => {
        this.setData({
          isShowDetail: true,
          senderCity: result.data.data.orderOutline.senderCity,
          consigneeCity: result.data.data.orderOutline.consigneeCity,
          orderNumber: result.data.data.orderOutline.orderNumber,
          items: result.data.data.items,
          centerLatitude: result.data.data.orderOutline.carLatitude,
          centerLongitude: result.data.data.orderOutline.carLongitude,
          carLatitude: result.data.data.orderOutline.carLatitude,
          carLongitude: result.data.data.orderOutline.carLongitude,
          consigneeLatitude: result.data.data.orderOutline.consigneeLatitude,
          consigneeLongitude: result.data.data.orderOutline.consigneeLongitude,
          markers:[
            {
                latitude:result.data.data.orderOutline.carLatitude,
                longitude: result.data.data.orderOutline.carLongitude,
                customCallout: {
                    "type": 2,
                    "descList": [{
                        "desc": result.data.data.days,
                        "descColor": "#108EE9"
                    }],
                    "isShow": 1,
                }
            },
            {
                latitude: result.data.data.orderOutline.consigneeLatitude,
                longitude: result.data.data.orderOutline.consigneeLongitude
            }
          ],
          polylines: {
            points: [
                {
                    latitude: result.data.data.orderOutline.carLatitude,
                    longitude: result.data.data.orderOutline.carLongitude
                },
                {
                    latitude: result.data.data.orderOutline.consigneeLatitude,
                    longitude: result.data.data.orderOutline.consigneeLongitude
                }
            ],
            color: "#eeeeee",
            width: 5,
            dottedLine: false
          }
        })
      },
      fail: (error) => {
          console.log(error)
          my.showToast({
            type: 'fail',
            content: '查询失败',
            duration: 1000
          });
      }
    });
  },
  scan(){
    my.scan({
      scanType: [
        'qrCode',
        'barCode'
      ],
      hideAlbum: true,
      success: (result) => {
        console.log(result)
        this.setData({
          orderNumber: result.barCode
        })
        this.onInputConfirm()
      },
      fail: () => {
        my.showToast({
          type: 'warn',
          content: '扫码失败',
          duration: 1000
        });
      }
    });
  },
  onLoad() {
    my.getNetworkType({
      success: (result) => {
        console.log(result)
        if(!result.networkAvailable){
          my.showToast({
            type: 'warn',
            content: '无网络连接',
            duration: 1000
          });
        }
      }
    });
  },
});
