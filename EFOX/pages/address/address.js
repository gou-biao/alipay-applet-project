const app = getApp()
const gb = app.globalData

Page({
  data: {
      list: []
  },
  ToEditAddress(e) {
    console.log(e)
    my.setStorage({
      key: 'addressId',
      data: {
          addrId: e.currentTarget.id
      },
      success: () => {
        my.navigateTo({
          url: '/pages/editAddress/editAddress',
          success: () => {
            console.log("跳转成功")
          },
          fail: () => {
            
          },
          complete: () => {
            
          }
        });
      },
      fail: () => {
        
      },
      complete: () => {
        
      }
    });
  },
  ToNewAddress(){
    my.navigateTo({
          url: '/pages/editAddress/editAddress'
    });
  },
  DeleteAddress(e){
    my.request({
      url: gb.url + '/address/deleteAddressById',
      method: 'GET',
      data: {
          addressId: e.currentTarget.id
      },
      success: (result) => {
        my.getStorage({
            key: 'userId',
            success: (result) => {
                my.request({
                  url: gb.url + '/address/getAddressListByUserId',
                  method: 'GET',
                  data: {
                      userId: result.data.userId
                  },
                  success: (result) => {
                        this.setData({
                            list: result.data.data
                        })
                  },
                  fail: () => {
                    my.showToast({
                      type: 'fail',
                      content: '更新列表失败',
                      duration: 1000
                    });
                  },
                  complete: () => {
                    
                  }
                });
            }
        });
        my.showToast({
          type: 'success',
          content: '删除成功',
          duration: 1000
        })
      },
      fail: () => {
        
      },
      complete: () => {
        
      }
    });
  },
  onLoad() {
        my.getStorage({
            key: 'userId',
            success: (result) => {
                my.request({
                  url: gb.url + '/address/getAddressListByUserId',
                  method: 'GET',
                  data: {
                      userId: result.data.userId
                  },
                  success: (result) => {
                        console.log(result)
                        this.setData({
                            list: result.data.data
                        })
                  }
                });
            }
        });
  },
  events: {
    onBack() {
    my.getStorage({
      key: 'userId',
      success: (result) => {
        my.request({
          url: gb.url + '/address/getAddressListByUserId',
          method: 'GET',
          data: {
              userId: result.data.userId
          },
          success: (result) => {
                console.log(result)
                this.setData({
                    list: result.data.data
                })
          }
        });
      }
    });
  }
  }
});
