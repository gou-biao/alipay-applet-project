const app = getApp();
const gb = app.globalData;

Page({
    data: {
        list: []
    },
    getOrderDetail(e){
        console.log(e)
        my.setStorage({
            key: 'transferOrderId',
            data: {
                orderId: e.currentTarget.id
            },
            success: () => {
                    my.navigateTo({
                    url: '/pages/orderDetail/orderDetail',
                    success: (res) => {
                        console.log("跳转成功")
                    }
                });
            },
            fail: () => {
                
            },
            complete: () => {
                
            }
        });
    },
    onLoad() {
        //   this.initOrderList()
        my.getStorage({
        key: 'userId',
        success: (result) => {
            console.log(result)
            my.request({
            url: gb.url + 'user/getUserOrderList',
            method: 'GET',
            data: {
                userId: result.data.userId
            },
            dataType: 'json',
            success: (result) => {
            this.setData({
                list: result.data.data
            })
            },
            fail: () => {
                console.log("获取订单信息失败")
            }
        });
        },
        fail: () => {
            
        },
        complete: () => {
            
        }
        });
    },
    onPullDownRefresh(){
        my.startPullDownRefresh({
            success: () => {
                console.log("xialashuaxin")
                my.getStorage({
                    key: 'userId',
                    success: (result) => {
                        console.log(result)
                        my.request({
                        url: gb.url + 'user/getUserOrderList',
                        method: 'GET',
                        data: {
                            userId: result.data.userId
                        },
                        dataType: 'json',
                        success: (result) => {
                            this.setData({
                                list: result.data.data
                            })
                        },
                        fail: () => {
                            console.log("获取订单信息失败")
                        }
                        });
                    },
                    fail: () => {
                        
                    },
                    complete: () => {
                        
                    }
                });
            },
            fail: () => {
            
            },
            complete: () => {
                my.stopPullDownRefresh();
            }
        });
    }   
});
