const app = getApp();
const gb = app.globalData;


Page({
    data: {
        orderNumber: "",
        senderCity: "",
        consigneeCity: "",
        transStatus: "4",
        centerLatitude: "",
        centerLongitude: "",
        carLatitude: "",
        carLongitude: "",
        consigneeLatitude: "",
        consigneeLongitude: "",
        markers: [],
        polylines: [],
        items:[],
        activeIndex: "1",
        failIndex: "0",
        showNumberSteps: true

    },
    moreDetail(){
        console.log("更多信息")
    },
    onReady() {
        console.log(my.getMapInfo)
        this.mapCtx = my.createMapContext('mymap');
        this.mapCtx.showsScale({isShowsScale:1});
    },
    onLoad() {
        my.getStorage({
            key: 'transferOrderId',
            success: (result) => {
                console.log(result.data)
                my.request({
                    url: gb.url + 'user/getOrderDetail',
                    method: 'GET',
                    data: {
                        orderId: result.data.orderId
                    },
                    dataType: 'json',
                    success: (result) => {
                        console.log(result)
                        this.setData({
                            senderCity: result.data.data.orderOutline.senderCity,
                            consigneeCity: result.data.data.orderOutline.consigneeCity,
                            orderNumber: result.data.data.orderOutline.orderNumber,
                            items: result.data.data.items,
                            centerLatitude: result.data.data.orderOutline.carLatitude,
                            centerLongitude: result.data.data.orderOutline.carLongitude,
                            carLatitude: result.data.data.orderOutline.carLatitude,
                            carLongitude: result.data.data.orderOutline.carLongitude,
                            consigneeLatitude: result.data.data.orderOutline.consigneeLatitude,
                            consigneeLongitude: result.data.data.orderOutline.consigneeLongitude,
                            markers:[
                                {
                                    latitude:result.data.data.orderOutline.carLatitude,
                                    longitude: result.data.data.orderOutline.carLongitude,
                                    customCallout: {
                                        "type": 2,
                                        "descList": [{
                                            "desc": result.data.data.days,
                                            "descColor": "#108EE9"
                                        }],
                                        "isShow": 1,
                                    }
                                },
                                {
                                    latitude: result.data.data.orderOutline.consigneeLatitude,
                                    longitude: result.data.data.orderOutline.consigneeLongitude
                                }
                            ],
                            polylines: {
                                points: [
                                    {
                                        latitude: result.data.data.orderOutline.carLatitude,
                                        longitude: result.data.data.orderOutline.carLongitude
                                    },
                                    {
                                        latitude: result.data.data.orderOutline.consigneeLatitude,
                                        longitude: result.data.data.orderOutline.consigneeLongitude
                                    }
                                ],
                                color: "#eeeeee",
                                width: 5,
                                dottedLine: false
                            }
                        })
                    },
                    fail: () => {
                        
                    },
                    complete: () => {
                        
                    }
                });
            },
            fail: () => {
                
            },
            complete: () => {
                
            }
        });
        console.log(this.mapCtx)
        
    }
});
